LIBRARY ieee;
USE ieee.std_logic_1164.all;
USE ieee.std_logic_arith.all;
USE ieee.std_logic_unsigned.all; --PC added

ENTITY MotorDriver IS
  
   PORT( 
      --INPUTS
		MotorA : IN std_logic;
		MotorADir : IN std_logic;
		MotorB : IN std_logic;
		MotorBDir : IN std_logic;
		--OUTPUTS
		Mode			: OUT		std_logic;
		MotorOut		: OUT 	unsigned(3 downto 0):= "0000"
		);

END MotorDriver;


architecture DriverV1 of MotorDriver is
begin
	Mode <= '1';
	MotorOut(0) <= MotorADir;
	MotorOut(1) <= MotorA;
		
	MotorOut(2) <= MotorBDir;
	MotorOut(3) <= MotorB;
		
end DriverV1;
