LIBRARY ieee;
USE ieee.std_logic_1164.all;
USE ieee.numeric_std.all;
use IEEE.STD_LOGIC_UNSIGNED.all;


--*****************************************
-- COLORTRACKER calculates CofM from camera pixel stream
-- for target colour, etc. 
-- ONLY ONE CHANNEL implemented at present
-- (c) Phil Culverhouse, 2014
-- University of Plymouth.
-- p.culverhouse@plymouth.ac.uk
--
-- All rights reserved
-- 
-- CHANGES: 
-- 
--********************************************

ENTITY ColourTracker IS
  
   PORT( 
      --INPUTS 
      clk    : IN     std_logic;
      rst    : IN     std_logic;
      VSYNC    : IN     std_logic;
		U : in unsigned (7 DOWNTO 0); -- pixel data channels
		V : in unsigned (7 DOWNTO 0);
		Y : in unsigned (7 DOWNTO 0);
		ROW : in unsigned (8 DOWNTO 0); -- counter for rows
		COLUMN : in unsigned (10 DOWNTO 0);  -- counter for columns
		Train		 : IN 	 std_logic; -- true only for training frame
		Accumulate : in std_logic; -- true only for valid ROWS (0-479) of data. 
		Calculate: in std_logic;  -- true for last row in VGA sequence (row 480)
		WinROW : in std_logic; -- true only for central window
		WinCOL : in std_logic; -- true only for central window
		YUVclkEN : in std_logic; -- true at the end of the 4 byte seq. use to clock in all data bytes from camera de-serialiser
      --OUTPUTS
		VisionPos : OUT integer range -10 to 10;
		VisionSize : OUT integer range 0 to 4;
		VisionFound : OUT std_logic := '0'
  );

-- Declarations

END ColourTracker ;

-- Colour tracker both training and test is similar activities
--u and v = 116 is RED
--u =102 and v = 119 is ORANGE GOLF BALL
ARCHITECTURE ColourTrackerV1 OF ColourTracker IS
	shared variable ySum : integer range 0 to 637500 := 0; --Dual usage, used for the sum and repurposed for tol check
	shared variable uSum : integer range 0 to 637500 := 0;
	shared variable vSum : integer range 0 to 637500 := 0;
	shared variable sumTotal : integer range    0 to 2500 := 0;
	shared variable foundColumn : integer range 0 to 637500 := 0;
	shared variable foundTotal : integer range 0 to 819200 := 0;
	shared variable doneCalc : std_logic := '0';
begin
	process(YUVclkEN)
	begin
		if rising_edge(YUVclkEN) then
			if (Train = '1') then
				-- Calculate the average color of the training window
				if (ROW = 1 and COLUMN = 1) then
					ySum := 0;
					uSum := 0;
					vSum := 0;
					sumTotal := 0;
				elsif (WinROW = '1' and WinCOL = '1') then
					--Training and in the training window
					--Average and sum the color components
					ySum := ySum + to_integer(Y);
					uSum := uSum + to_integer(U);
					vSum := vSum + to_integer(V);
					sumTotal := sumTotal + 1;
				end if;
			else
				if (Accumulate = '1' and doneCalc = '1') then
					foundColumn := 0;
					foundTotal := 0;
					doneCalc := '0';
				end if;
			
				if ((ySum + 10) < to_integer(Y) and (ySum - 10) > to_integer(Y)) and ((uSum + 5) < to_integer(U) and (uSum - 5) > to_integer(U)) and ((vSum + 5) < to_integer(V) and (vSum - 5) > to_integer(V)) then
				--if ((uSum + 4) > to_integer(U) and (uSum - 4) < to_integer(U)) and ((vSum + 4) > to_integer(V) and (vSum - 4) < to_integer(V)) then
				--if ((111 + 5) > to_integer(U) and (111	- 5) < to_integer(U)) and ((116 + 5) > to_integer(V) and (116 - 5) < to_integer(V)) then
					--Pixel is the correct color
					foundColumn := foundColumn + to_integer(COLUMN);
					foundTotal := foundTotal + 1;
				end if;
				
				if (Calculate = '1') and doneCalc = '0' then
					--Time to calculate tings
					doneCalc := '1';
					
					if (foundTotal > 110) then
						foundColumn := foundColumn / foundTotal;
						
						if (FoundColumn > 640) then
							--On the right
							VisionPos <= (FoundColumn - 640) / 64;
						else
							--On the left
							VisionPos <= -(FoundColumn / 64);
						end if;
						
						if (foundTotal > 34000) then
							VisionSize <= 4;
						elsif (foundTotal > 8500) then
							VisionSize <= 3;
						elsif (foundTotal > 2100) then
							VisionSize <= 2;
						else
							VisionSize <= 1;
						end if;
						
						VisionFound <= '1';
						
					else
						VisionPos <= 0;
						VisionSize <= 0;
						VisionFound <= '0';
					end if;

					
					if (sumTotal > 100) then
						--Calculate the average of the training frame
						ySum := ySum / sumTotal;
						uSum := uSum / sumTotal;
						vSum := vSum / sumTotal;
						sumTotal := 0;
					end if;
				end if;
			end if;
		end if;
	end process;
end architecture;