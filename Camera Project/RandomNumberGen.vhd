LIBRARY ieee;
USE ieee.std_logic_1164.all;
USE ieee.std_logic_arith.all;
USE ieee.std_logic_unsigned.all; --PC added

ENTITY RandomNumberGen IS
  
   PORT( 
      --INPUTS
		clock : IN std_logic;
		enable : IN std_logic;
		--OUTPUTS
		output : OUT integer range 0 to 65535 := 0
	);

END RandomNumberGen;

architecture RandomNumberGenV1 of RandomNumberGen is

	shared variable Seed : integer := 1234;

begin
	
	process (clock)
	begin
		if (rising_edge(clock) and enable = '1') then
			Seed := (1242 * Seed + 1234) mod 3452;
			output <= Seed;
		end if;
	end process;

end RandomNumberGenV1;