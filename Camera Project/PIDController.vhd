LIBRARY ieee;
USE ieee.std_logic_1164.all;
USE ieee.std_logic_arith.all;
USE ieee.std_logic_unsigned.all; --PC added

ENTITY PIDController IS
  
   PORT( 
      --INPUTS
		clock : IN std_logic;
		SetPoint : IN integer range -200 to 200;
		ProcessVariable : IN integer range 0 to 300;
		--OUTPUTS
		ControlOutput : OUT integer range -100 to 100
	);

END PIDController;

architecture PIDControllerV1 of PIDController is
	constant KP : integer := 2; -- KP (P gain)
	constant KI : integer := 5; -- Reciprical KI (I gain)
	constant KD : integer := 25; -- Reciprical KD (D gain)
	
	signal Error : integer := 0;
	signal LastError : integer := 0;
	
	shared variable OutputStorage : integer := 0;
	shared variable Integral : integer := 0;
	shared variable Derivitive : integer := 0;
begin
	
	process (clock)
	begin
		LastError <= Error; --Use signals as buffer
		Error <= ABS(SetPoint) - ProcessVariable;
		
		Integral := Integral + (Error / 400000); --200khz is clock
		Derivitive := (Error - LastError) * 400000; --200khz is clock
		
		OutputStorage := (Error * KP) + (Integral / KI) + (Derivitive / KD);
		
		if (ABS(SetPoint) = 0) then
			Integral := 0; --To prevent integral windup
			ControlOutput <= 0; --For quick stop
		else
			if (OutputStorage < 30) and (ABS(SetPoint) > 40) then
				OutputStorage := 20;
			end if;
			
			if (OutputStorage > 125) then
				OutputStorage := 125;
			elsif (OutputStorage < 0) then
				--Sanity check
				OutputStorage := 0;
			end if;
			
			if (SetPoint < 0) then
				ControlOutput <= 0 - (OutputStorage - (OutputStorage / 5)); -- Output * 0.8 is transfer function
			else
				ControlOutput <= OutputStorage - (OutputStorage / 5); -- Output * 0.8 is transfer function
			end if;
		end if; 
	end process;

end PIDControllerV1;