LIBRARY ieee;
USE ieee.std_logic_1164.all;
USE ieee.std_logic_arith.all;
USE ieee.std_logic_unsigned.all; 

--*****************************************
-- SM1 generates i2c command stream for camera etc.
-- sends the contents of a ROM with the specific register contents
-- (c) Phil Culverhouse, 2007
-- University of Plymouth.
-- p.culverhouse@plymouth.ac.uk
--
-- All rights reserved
-- 
-- CHANGES: 
-- 
--********************************************

ENTITY SM1 IS
generic (ROMwords: unsigned (4 downto 0) :="01100");
  
   PORT( 
      --INPUTS
      clk    : IN     std_logic;
      rst    : IN     std_logic;
		go		 : IN 	 std_logic;
		i2c_END: in std_logic; -- from i2c controller
      --OUTPUTS
		i2c_reset: out std_logic; -- to i2c controller to clr to reset state, ready to go
		i2c_go: out std_logic; -- to i2c controller to enable next word send
		sel:out std_logic; -- mux select, either camera or VCM data send
      ROMaddr  : OUT    unsigned (4 DOWNTO 0); -- this is incremented for each i2c_END pulse
		ROM_addr_incr: out std_logic;
		CAM_reset: out std_logic; -- 1ms reset for camera
		TX_32_24: out std_logic -- 32/24 bit TX i2c 
   );

-- Declarations

END SM1 ;

-- Not really a State Machine, rather a counter.
ARCHITECTURE arc OF SM1 IS

type SM1_master_state is ( SM1_idle, SM1_state1,SM1_state1a,SM1_state2,SM1_state2a,SM1_state3, SM1_state4,
									SM1_state5,SM1_state6,SM1_state6a,SM1_state7,SM1_state7a,SM1_state8,SM1_state9,SM1_state10,SM1_state11,SM1_state12,SM1_state13);
		
  signal i_CAM_reset_cnt: natural range 0 to 400:=0 ; -- ensure camera reset is 1ms long
  signal i_CAM_reset_cnt_en:std_logic:='0';
  signal i_CAM_reset:std_logic:='0';
  signal i_go_latched:std_logic:='0'; -- a i2c go signal controlled by the reset system (with 1ms delay)

  signal stm_SM1: SM1_master_state:=SM1_idle;
  signal i_cnt: unsigned (4 downto 0):="00000";
  signal  i_cnt_en: std_logic:='0';
  signal i_start: std_logic:='0';
  signal i_stop: std_logic:='0';

begin 

ROMaddr <= i_cnt;
CAM_reset <= i_CAM_reset;
process (clk,rst)
begin
	if (rst='1') then -- sync reset when NOT system reset
		i_go_latched <='0'; -- a latched go signal for the i2c system
		i_CAM_reset_cnt<=0;  -- reset count value
		i_CAM_reset_cnt_en<='1'; -- enable count
		i_CAM_reset<='1';
	elsif (clk'event and clk='1') then
		if (i_CAM_reset_cnt_en='1') then
			i_CAM_reset_cnt<= i_CAM_reset_cnt+1;
			i_CAM_reset<='0';
			i_go_latched <='0'; -- a latched go signal for the i2c system
			
			if (i_CAM_reset_cnt > 400) then
				i_CAM_reset<='1';
				i_go_latched <='1'; -- a latched go signal for the i2c system
				i_CAM_reset_cnt<=0;
				i_CAM_reset_cnt_en<='0'; -- disable the counter until the next reset
			end if;
		else
			i_CAM_reset<='1';
			i_go_latched <='0'; -- a latched go signal for the i2c system
			i_CAM_reset_cnt<=0;
		end if;
	end if;
end process;

-- convert GO into the start of a sequence of start-stop cycles for each 32 bit i2c word sent by i2c_controller.v
process (clk, rst,i_CAM_reset)
begin
	  if (i_CAM_reset='0') then-- wait for 1ms counter to drive camera reset
		TX_32_24<='1'; --make 32bit
				sel<= '0';
				i2c_go<='0';
				i2c_reset<='0';
				i_cnt_en<='0';
				ROM_addr_incr <='0';
			stm_SM1<=SM1_idle; --  stm_SM4
	elsif (clk ='1' and clk'event) then
		i2c_reset<='0';
		case stm_SM1 is
		--============================== IDLE
		when SM1_idle =>
		TX_32_24<='1'; --make 32bit
				if (go='1') then  
				sel<= '0';
				i2c_go<='1';
				i2c_reset<='1';
				i_cnt_en<='0';
				ROM_addr_incr <='0';
				stm_SM1<=SM1_state1; -- start i2c	
				else
				sel<= '0';
				i2c_go<='0';
				i2c_reset<='1';
				i_cnt_en<='0';
				ROM_addr_incr <='0';
				stm_SM1<= SM1_idle;
				end if;	
		--============================== send go pulse to i2c_controller
		when SM1_state1 => 
		TX_32_24<='1'; --make 32bit
				sel<= '0';
				i2c_go<='1';
				i2c_reset<='1';
				i_cnt_en<='0';
				ROM_addr_incr <='0';
				stm_SM1<= SM1_state2; 
		--============================== a gap
		when SM1_state2 => 
		TX_32_24<='1'; --make 32bit
				sel<= '0';
				i2c_go<='1';
				i2c_reset<='1';
				i_cnt_en<='0';
				ROM_addr_incr <='1';
				stm_SM1<= SM1_state3; 
		--============================== wait here for i2c_END from i2c_controller
		when SM1_state3 => 
		TX_32_24<='1'; --make 32bit
			if (i2c_END='1') then -- got to end of 32 bit i2c write, incr ROM addr
				sel<= '0';
				i2c_go<='0';
				i2c_reset<='1';
				i_cnt_en<='1';
				ROM_addr_incr <='0';
				stm_SM1<= SM1_state4; 
			else
				sel<= '0';
				i2c_go<='1';
				i2c_reset<='1';
				i_cnt_en<='0';
				ROM_addr_incr <='0';
				stm_SM1<= SM1_state3; 
			end if;
		--============================== send reset pulse to i2c_controller to clear i2c_END
		when SM1_state4 => 
		TX_32_24<='1'; --make 32bit
			if (i_stop='1') then -- got to end of 32 bit i2c write, incr ROM addr
				sel<= '0';
				i2c_go<='0';
				i2c_reset<='0';
				i_cnt_en<='0';
				ROM_addr_incr <='0';
--PC ov7670				stm_SM1<= SM1_state5; 
				stm_SM1<= SM1_idle; -- when with OV7670FSL camera just go back no auto-focus i2c data
			else
				sel<= '0';
				i2c_go<='0';
				i2c_reset<='1';
				i_cnt_en<='0';
				ROM_addr_incr <='0';
				stm_SM1<= SM1_state2; 
			end if;
--============================= now control the VCM ad infinitum
		when SM1_state5 => 
		TX_32_24<='0'; --make 24 bit
				sel<= '1';
				i2c_go<='1';
				i2c_reset<='1';
				i_cnt_en<='0';
				ROM_addr_incr <='0';
				stm_SM1<= SM1_state6; 
		--============================== a gap
		when SM1_state6 => 
		TX_32_24<='0'; --make 24 bit
				sel<= '1';
				i2c_go<='1';
				i2c_reset<='1';
				i_cnt_en<='0';
				ROM_addr_incr <='0';
				stm_SM1<= SM1_state7; 
		--============================== wait here for i2c_END from i2c_controller
		when SM1_state7 => 
		TX_32_24<='0'; --make 24 bit
			if (i2c_END='1') then -- got to end of 32 bit i2c write, incr ROM addr
				sel<= '1';
				i2c_go<='0';
				i2c_reset<='1';
				i_cnt_en<='1';
				ROM_addr_incr <='1';
				stm_SM1<= SM1_state5; 
			else
				sel<= '1';
				i2c_go<='1';
				i2c_reset<='1';
				i_cnt_en<='0';
				ROM_addr_incr <='0';
				stm_SM1<= SM1_state7; 
			end if;			
			when others=>
		TX_32_24<='1'; --make 32 bit
				sel<= '0';
				i2c_go<='0';
				i2c_reset<='1';
				i_cnt_en<='0';
			ROM_addr_incr <='0';
				stm_SM1<= SM1_idle;
	
		end case;
	end if;
end process;

-- counter that increments to another ROM address. 
process (clk,rst, i_cnt_en)
begin
	if (rst='1') then
		i_cnt<="00000";
		i_stop<='0';
	elsif (clk'event and clk='1' and  i_cnt_en='1') then --- was... and rom_addr_incr='1') then
		if (i_cnt<ROMwords-1) then
			i_stop<='0';
			i_cnt <= i_cnt+1;
		else
			i_cnt <= "00000";
			i_stop<='1'; -- tell the SM to stop and exit.
		end if;
	end if;
end process;
end architecture;