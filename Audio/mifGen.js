var fs = require('fs');
var readStream = fs.createReadStream('compressed.wav');
var line = '';
var lineCounter = 0;
var rowCounter = 0;

var output = '';
readStream.on('data', function (chunk) {
  /*chunk = chunk.toString('hex');

  for (var key = 88; key < chunk.length; key++) {
    if (lineCounter > 1) {
      console.log(rowCounter, rowCounter.toString(16));
      output += rowCounter.toString(16) + ' : ' + line + ";\r\n";
      lineCounter = 0;
      line = '';
      rowCounter ++;
    }
    line += chunk[key];
    lineCounter ++;
  }*/

  for (var key = 88; key < chunk.length; key++) {
    /*var num;
    if (chunk[key] > 127) {
      num = chunk[key] / 127;
    } else {
      num = -((128 - chunk[key]) / 127);
    }*/

    var num = chunk[key];

    num *= 1.5;

    if (num > 255) num = 255;

    num = parseInt(num);

    console.log(rowCounter, num);
    output += rowCounter.toString(16) + ' : ' + num + ";\r\n";
    //output += rowCounter + ',' + num + "\r\n";

    rowCounter ++;

  }

});
readStream.on('end', function () {
  /*if (lineCounter != 0) {
    output += rowCounter.toString(16) + ' : ' + line + ";\r\n";
    console.log(rowCounter);
  }*/
  output += 'END;';

  var stream = fs.createWriteStream('audio.mif', {flags: 'w'});
  stream.write("DEPTH = " + rowCounter + ";\r\n");
  stream.write("WIDTH = " + 8 + ";\r\n");
  stream.write("ADDRESS_RADIX = HEX;\r\n");
  stream.write("DATA_RADIX = UNS;\r\n");
  stream.write("CONTENT BEGIN\r\n");
  stream.write(output);

});
