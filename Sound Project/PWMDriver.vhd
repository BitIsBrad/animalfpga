LIBRARY ieee;
USE ieee.std_logic_1164.all;
USE ieee.std_logic_arith.all;
USE ieee.std_logic_unsigned.all;

ENTITY PWMDriver IS
	PORT( 
      --INPUTS
      speed : IN integer range -100 to 100 := 100;
		Clock400K : IN std_logic;
      --OUTPUTS
      enable : OUT std_logic;
		direction : OUT std_logic
   );
	
end PWMDriver;

architecture PWMDriverV1 of PWMDriver is
	shared variable internal_count : integer := 0;
begin
	process (Clock400K)
	begin
		if rising_edge(Clock400K) then
			if (internal_count > 100) then
				internal_count := 0;
			end if;

			internal_count := internal_count + 1;
		end if;
		
		if (speed = 0) then
			enable <= '0';
			direction <= '0';
			
		elsif (speed > 0) then
			direction <= '0';
			
			if (internal_count < speed) then
				enable <= '1';
			else
				enable <= '0';
			end if;
		
		elsif (speed < 0) then
			direction <= '1';
			
			if (internal_count < ABS(speed)) then
				enable <= '1';
			else
				enable <= '0';
			end if;
		
		end if;
		
	end process;
end PWMDriverV1;
