--*****************************************
-- (c) Phil Culverhouse, 2006
-- University of Plymouth.
-- p.culverhouse@plymouth.ac.uk
--
-- All rights reserved
-- 
-- CHANGES:
-- 30/07/2008 - BENOIT QUENTIN - Replace Servo_v2 componant by preparePWM
--********************************************

LIBRARY ieee;
USE ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;


ENTITY SET_NeckServos_v1 IS
	PORT
	(
		Clk 		: IN  std_logic;
		Aclr		: IN std_logic;
		LNeckPOS	: IN STD_LOGIC_VECTOR (12 downto 0); -- +/- 128 rotation control of servos
		RNeckPOS	: IN STD_LOGIC_VECTOR (12 downto 0); -- +/- 128 rotation control of servos
		CycStart	: OUT std_logic;
		PWM1		: out std_ulogic;
		PWM2		: out std_ulogic
	);
END SET_NeckServos_v1;

ARCHITECTURE Cyclone OF SET_NeckServos_v1 IS

	signal CycStart1, CycStart2: std_logic;
	signal PWM_MIN1, PWM_MAX1 : STD_LOGIC_VECTOR(12 DOWNTO 0);
	signal PWM_MIN2, PWM_MAX2 : STD_LOGIC_VECTOR(12 DOWNTO 0);
	signal Bias1, Bias2 : STD_LOGIC_VECTOR(12 DOWNTO 0);
	
	COMPONENT preparePWM
		PORT
		(
			PWM_Min		:	 IN STD_LOGIC_VECTOR(12 DOWNTO 0);
			PWM_Max		:	 IN STD_LOGIC_VECTOR(12 DOWNTO 0);
			Offset		:	 IN STD_LOGIC_VECTOR(12 DOWNTO 0);
			Bias		:	 IN STD_LOGIC_VECTOR(12 DOWNTO 0);
			clk			:	 IN STD_LOGIC;
			aclr		:	 IN STD_LOGIC;
			PWM			:	 OUT STD_LOGIC;
			CycStart	:	 OUT STD_LOGIC
		);
		END COMPONENT;
					
-----------------------------------------------------------			
begin
			
-- SET SERVO BIASES FOR MID-POINTS HERE FOR EACH ROBOT ----

PWM_MIN1 <= conv_std_logic_vector(-20,13);
PWM_MAX1 <= conv_std_logic_vector(18,13);
Bias1 <= conv_std_logic_vector(7,13);

PWM_MIN2 <= conv_std_logic_vector(-60,13);
PWM_MAX2 <= conv_std_logic_vector(60,13);
Bias2 <= conv_std_logic_vector(8,13);
			
CycStart <= CycStart1; -- monitor first servo
				
u1: preparePWM PORT MAP (PWM_MIN1, PWM_MAX1, LNeckPOS(12 downto 0), Bias1, clk, aclr, PWM1, CycStart1);  			

u2: preparePWM PORT MAP (PWM_MIN2, PWM_MAX2, RNeckPOS(12 downto 0), Bias2, clk, aclr, PWM2, CycStart2);
	
	---------------------------------------------------------------------------
END Cyclone;