LIBRARY ieee;
USE ieee.std_logic_1164.all;
USE ieee.std_logic_arith.all;
USE ieee.std_logic_unsigned.all; --PC added

ENTITY HallEffectTrigger IS
  
   PORT( 
      --INPUTS
		Clock400K : IN std_logic; -- 400khz
		HallA 	: 	IN std_logic;
		HallB 	: 	IN std_logic;
		Reset		: 	IN std_logic;
		--OUTPUTS
		Distance : OUT integer range 0 to 10000;
		Speed : OUT integer range 0 to 300;
		Direction : OUT std_logic
	);

END HallEffectTrigger;


architecture HallEffectTriggerV1 of HallEffectTrigger is
	shared variable Counter : integer := 0;
	shared variable SpeedCounter : integer := 0;
	signal CurrentA : std_logic;
	signal CurrentB : std_logic;
	signal LastA : std_logic;
	signal LastB : std_logic;
begin
	process(Clock400K, Reset)
		begin
			if (Reset = '1') then
				CurrentA <= '0';
				CurrentB <= '0';
				LastA <= '0';
				LastB <= '0';
				Counter := 0;
				Distance <= Counter;
			elsif (rising_edge(Clock400K)) then
				LastA <= CurrentA;
				LastB <= CurrentB;
			
				CurrentA <= HallA;
				CurrentB <= HallB;
				
				if (CurrentA = '1') then
					SpeedCounter := SpeedCounter + 1;
					If (LastA = '0') then
						--If rising edge on quadrature a
						SpeedCounter := 0;
					end if;
				end if;
				
				if (SpeedCounter > 162162) then
					Speed <= 0;
					SpeedCounter := 162162;
				end if;
				
				if (CurrentA = '0' and LastA = '1') then
					--If Falling edge on quadrature a
					Speed <= (162162 / SpeedCounter);
					if (CurrentB = '1') then
						Direction <= '1';
						Counter := Counter + 1;
					elsif (CurrentB = '0') then
						Direction <= '0';
						Counter := Counter + 1;
					end if;
				end if;
				
				Distance <= Counter;
				
			end if;
	end process;
		
end HallEffectTriggerV1;
