--*****************************************
-- (c) Phil Culverhouse, 2007
-- University of Plymouth.
-- p.culverhouse@plymouth.ac.uk
--
-- All rights reserved
-- 
-- CHANGES: 
-- version 2 now clks all counters from the 50MHz original, and not the cascaded clk division
-- to try and reduce jitter on the servos.
--********************************************


--*******************************
-- Last change:
-- 09.08.2008 : Ben QUENTIN
--*******************************



LIBRARY ieee;
USE ieee.std_logic_1164.all;
-- not both this and below -- USE ieee.std_logic_arith.all;
--use IEEE.numeric_bit.all; -- for integer to bit_vector conversion
use IEEE.numeric_std.all; -- for integer to bit_vector conversion

--PWM timing
--		0.02	clk
--		11.72	586	cout_4us
--		1500.16	128	offset
--		20029.48	1709	cout_20ms	
--		for some reason 1789 works 	


ENTITY Servo_v2 is
generic (PWM_MIN: signed (12 downto 0):= to_signed(-63,13); 
		 PWM_MAX: signed (12 downto 0):= to_signed(+64,13));

	PORT
	(
		Clk 		: IN  std_logic;
		Aclr		: IN std_logic;
		Offset		: in signed (12 downto 0); -- +/- 128 rotation control of servos
		Bias		: in signed (12 downto 0); -- small centre point asjustment for each servo, to account for spline position
		CycStart	: OUT std_logic;
--		osc			: out std_logic; -- 4us clk s
		PWM			: out std_ulogic
	);
END Servo_v2;


-- first register the pixel stream
ARCHITECTURE Cyclone OF Servo_v2 IS

	constant  baseWIDTH : signed := to_signed(187,13);--"0000010111011"; -- was 1.08ms! -->"0001111110"; -- 1.5 ms centres servos +/- 0.5ms controls +/- 45 deg around this.
	constant  baseMIN : signed := to_signed(125,13);--"0000001111101"; -- was 0.67 ms "0001010100"; -- 1.0 ms min PWM
	signal count: signed (12 downto 0) := baseWIDTH; -- Current & Next state variables
	signal count_10_signed: signed (12 downto 0); -- Current & Next state variables
	signal count20ms: signed (12 downto 0) := "0000000000000"; -- Current & Next state variables
	signal cout_20ms: std_logic := '0';
	signal count4us: signed (12 downto 0) := "0000000000000"; -- Current & Next state variables
	signal cout_4us: std_logic := '0';
	signal cout_PWM : std_logic := '0';
	signal offset_OK: signed (12 downto 0); -- holds user offest that has been constrained by specific servo rotation constraints
	
begin
	---------------------------------------------------------------------------


CycStart <= cout_20ms;
--Q <= count;
--osc <= cout_4us;
CHK_offset: process (offset) -- ensure we do not overdrive specific servo
	begin
		if (signed(offset) > signed(PWM_MAX)) then
			offset_OK <= PWM_MAX;
		elsif (signed(offset) < signed(PWM_MIN)) then
			offset_OK <= PWM_MIN;
		else
			offset_OK <= offset; -- its OK drive user value
		end if;
end process CHK_offset;

FourUS_clock: process (clk, aclr) -- clk tick every 4us
	begin
		if clk'event and clk='1' then
			if aclr = '1'  then  -- reload counter for next PWM signal
				count4us <= "0000000000000";
				cout_4us <= '0'; 
			end if;
--BQ 09.06.08			if count4us > 586 then -- carefully calc. to give 1-2ms range with 8 bit resolution of PWM - DO NOT ALTER
			if count4us > (400-2) then -- carefully calc. to give 1-2ms range with 8 bit resolution of PWM - DO NOT ALTER
				cout_4us <= '1';		
				count4us <= "0000000000000";			
			else	
				count4us <= count4us + 1;			
				cout_4us <= '0'; 
			end if;
		end if;
end process FourUS_clock;	
	
TWOZEROms_clock: process (cout_4us, aclr) -- clk tick every 4us
	begin
		if cout_4us'event and cout_4us='1' then
			if aclr = '1'  then  -- reload counter for next PWM signal
				count20ms <= "0000000000000";
			end if;
--BQ 09.06.08			if count20ms >  "11011111101" then -- for some reason 1789 works!
			if count20ms >  (2500-2) then
				cout_20ms <= '1';		
				count20ms <= "0000000000000";			
			else	
				count20ms <= count20ms + 1;
				cout_20ms <= '0';			
			end if;
		end if;
end process TWOZEROms_clock;	

			
Offset_CTRL:	process (aclr, cout_4us, cout_20ms) -- adds width to the 20ms time marks
	begin
		if (cout_4us'event and cout_4us='1') then
			count <= count - 1;
			if  aclr = '1'  or cout_20ms = '1' then  -- reload counter for next PWM signal 
				count_10_signed <= (signed(baseWIDTH) + signed(Bias) + signed(offset_OK));
				if count_10_signed < signed(baseMIN) then
					count <= signed(baseMIN); -- do not let the counter go below the 1ms period to avoid servo rotation
				else
					count <= count_10_signed - 2; -- -2 because start after 8us en go to 0
				end if;
			end if;
			if count = 0 then
				cout_PWM <= '1';		
			else	
				cout_PWM <= '0'; 
			end if;
		end if;
end process Offset_CTRL;

PWM_CTRL: process (aclr, cout_20ms, cout_PWM)
	begin
			if (cout_20ms'event and cout_20ms='1') then 
				PWM <= '1';
			end if;
			if (aclr ='1' or cout_PWM = '1') then
				PWM <= '0';
				-- count until 
			end if;
end process PWM_CTRL;	
	
	---------------------------------------------------------------------------
END Cyclone;