LIBRARY ieee;
USE ieee.std_logic_1164.all;
USE ieee.numeric_std.all;

--*****************************************
-- DACDAT generates digital serial audio signals for the WM8974 codec
--
-- (c) Phil Culverhouse, William Stephenson 2014
-- University of Plymouth.
-- p.culverhouse@plymouth.ac.uk
--
-- All rights reserved
-- 
-- CHANGES: 
-- 
--********************************************

ENTITY DACDAT IS
  
   PORT( 
      --INPUTS
      clk    : IN     std_logic;
      rst    : IN     std_logic;
		FRAME: in std_logic; -- assume WM8974 is master (set in i2c configuration)
		MemoryIn : in STD_LOGIC_VECTOR (7 DOWNTO 0);
		PlayTrigger : IN std_logic; --Used to toggle audio playback
		PlayMode : IN std_logic;	--Used to set playback sound (0 = wav file, 1 = tone)
      --OUTPUTS
		DAC: out std_logic; -- serial digital audio output to codec
		ReadAddress : out STD_LOGIC_VECTOR (13 DOWNTO 0) --Used to controll the memory controller
   );

-- Declarations

END DACDAT ;

-- a 16-bit serial data stream generator,
-- currently just an audio freq. sawtooth wave of feq. 183Hz (approx)

ARCHITECTURE arc OF DACDAT IS
signal N: natural range 0 to 63; --points to the correct bit in the serialised word

shared variable SoundPos : integer range 0 to 9591 := 0; --859526
shared variable SlowCounter : integer range 0 to 2 := 0;

shared variable ReadScaled : STD_LOGIC_VECTOR (15 DOWNTO 0);



begin 

-- counter that makes a ramp up and down ie. sawtooth wave for DAC audio o/p tests
process (clk, rst, FRAME)
begin

	if (rst='1') then 
		SoundPos := 0; --Reset the playback position
	elsif (FRAME'event and FRAME='1') then
	
		SlowCounter := SlowCounter + 1; --Count to 3 to fix playback rate to 8000 hz (input is 24000hz)
		
		if (SlowCounter > 2) then --If timing is for 8000hz
			SlowCounter := 0; --Reset slow
			SoundPos := SoundPos + 1; --Increment wav file position
			
			if (SoundPos = 9591) then --If at the end of the file restart
				SoundPos := 0;
			end if;
			
			ReadScaled := (others => '0'); --Set output as nothing
			
			if (PlayTrigger = '1') then --Check if playing
				if (PlayMode = '0') then
					-- If playback file
					ReadScaled(15 downto 8) := MemoryIn; --Scale 8 bit to 16 bit output (from rom)
				else
					--if playback tone
					if (SoundPos > 80) then --Loop for 240hz
						SoundPos := 0;
					end if;
					
					--Playback at around half volume sawtooth wave
					ReadScaled := std_logic_vector(to_unsigned(SoundPos * 400, 16)); --819 is full volume
				end if;
			end if;
			
		end if;

	elsif (FRAME'event and FRAME='0') then
		--Set the memory address to the correct position
		--So audio bits are realy to load
		ReadAddress <= std_LOGIC_VECTOR(to_unsigned(SoundPos, 14));
	end if;
end process;

DAC <= ReadScaled(N); -- drive the DACDAT pin

-- serial shift register to output count value to DACDAT
process (clk,rst, FRAME)
begin
	if (rst='1' or FRAME='0') then 
		-- SO MSB BIT FIRST is required in our default Codec configuration #############
		N <= 0;
	elsif (clk'event and clk='0' and FRAME='1' and N < 15) then
		N <= N + 1;
	end if;
end process;


end architecture;