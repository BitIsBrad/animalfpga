--*****************************************
-- (c) Phil Culverhouse, 2006
-- University of Plymouth.
-- p.culverhouse@plymouth.ac.uk
--
-- All rights reserved
-- 
-- CHANGES:
-- 30/07/2008 - BENOIT QUENTIN - Replace Servo_v2 componant by preparePWM
--********************************************

LIBRARY ieee;
USE ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;


ENTITY SET_EyeServos_v1 IS
	PORT
	(
		Clk 		: IN  std_logic;
		Aclr		: IN std_logic;
		LeyePOSlr	: IN signed (12 downto 0); -- +/- 128 rotation control of servos
		LeyePOSud	: IN signed (12 downto 0); -- +/- 128 rotation control of servos
		ReyePOSlr	: IN signed (12 downto 0); -- +/- 128 rotation control of servos
		ReyePOSud	: IN signed (12 downto 0); -- +/- 128 rotation control of servos
		CycStart	: OUT std_logic;
		PWM5		: out std_ulogic;
		PWM6		: out std_ulogic;
		PWM7		: out std_ulogic;
		PWM8		: out std_ulogic
	);
END SET_EyeServos_v1;

ARCHITECTURE Cyclone OF SET_EyeServos_v1 IS

	signal CycStart5, CycStart6, CycStart7, CycStart8: std_logic;
	signal PWM_MIN5, PWM_MAX5 : STD_LOGIC_VECTOR(12 DOWNTO 0);
	signal PWM_MIN6, PWM_MAX6 : STD_LOGIC_VECTOR(12 DOWNTO 0);
	signal PWM_MIN7, PWM_MAX7 : STD_LOGIC_VECTOR(12 DOWNTO 0);
	signal PWM_MIN8, PWM_MAX8 : STD_LOGIC_VECTOR(12 DOWNTO 0);
	signal Bias5, Bias6, Bias7, Bias8 : STD_LOGIC_VECTOR(12 DOWNTO 0);
	
	COMPONENT preparePWM
		PORT
		(
			PWM_Min		:	 IN STD_LOGIC_VECTOR(12 DOWNTO 0);
			PWM_Max		:	 IN STD_LOGIC_VECTOR(12 DOWNTO 0);
			Offset		:	 IN signed(12 DOWNTO 0);
			Bias		:	 IN STD_LOGIC_VECTOR(12 DOWNTO 0);
			clk			:	 IN STD_LOGIC;
			aclr		:	 IN STD_LOGIC;
			PWM			:	 OUT STD_LOGIC;
			CycStart	:	 OUT STD_LOGIC
		);
		END COMPONENT;
			
begin
			
-- SET SERVO BIASES FOR MID-POINTS HERE FOR EACH ROBOT ----

PWM_MIN5 <= conv_std_logic_vector(-18,13);
PWM_MAX5 <= conv_std_logic_vector(18,13);
Bias5 <= conv_std_logic_vector(-12,13);

PWM_MIN6 <= conv_std_logic_vector(-28,13);
PWM_MAX6 <= conv_std_logic_vector(28,13);
Bias6 <= conv_std_logic_vector(0,13);

PWM_MIN7 <= conv_std_logic_vector(-18,13);
PWM_MAX7 <= conv_std_logic_vector(18,13);
Bias7 <= conv_std_logic_vector(-1,13);

PWM_MIN8 <= conv_std_logic_vector(-28,13);
PWM_MAX8 <= conv_std_logic_vector(28,13);
Bias8 <= conv_std_logic_vector(0,13);
			
CycStart <= CycStart5; -- monitor first servo
				
u5: preparePWM PORT MAP (PWM_MIN5, PWM_MAX5, LeyePOSlr(12 downto 0), Bias5, clk, aclr, PWM5, CycStart5);  			

u6: preparePWM PORT MAP (PWM_MIN6, PWM_MAX6, LeyePOSud(12 downto 0), Bias6, clk, aclr, PWM6, CycStart6);

u7: preparePWM PORT MAP (PWM_MIN7, PWM_MAX7, ReyePOSlr(12 downto 0), Bias7, clk, aclr, PWM7, CycStart7);  			

u8: preparePWM PORT MAP (PWM_MIN8, PWM_MAX8, ReyePOSud(12 downto 0), Bias8, clk, aclr, PWM8, CycStart8);

---------------------------------------------------------------------------
END Cyclone;