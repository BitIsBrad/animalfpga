LIBRARY ieee;
USE ieee.std_logic_1164.all;
USE ieee.std_logic_arith.all;

ENTITY SM4 IS
  
   PORT( 
      --INPUTS
      clk    : IN     std_logic;
		clk_0400:IN std_logic; -- 100KHz slow clk for i2c interval counters
      rst    : IN     std_logic;
		go: IN std_logic;
		ROM_addr_incr : IN std_logic;
		ROM_end: IN std_logic; -- signals end of ROM addr counter, so all CAM init done
		ready: IN std_logic; -- i2c master handshake
		read_ID:IN std_logic; -- set true to read camera ID byte
      --OUTPUTS
      start      : OUT     std_logic;
      stop       : OUT     std_logic;
		i2c_reset:OUT std_logic;
		enCamCount: out std_logic;
		ROM_end_o: OUT std_logic;
      sel  : OUT    std_logic; -- 32 bit data now, just select CAMERA or ROM
		ROM_clk: OUT std_logic;
		read:OUT std_logic;
		write:OUT std_logic;
		resetZ:OUT std_logic -- drive '0' and 'Z' onlyk never '1' to ensure NANO does not overdrive camera volatage
   );

-- Declarations

END SM4 ;

--
ARCHITECTURE arc OF SM4 IS
type SM4_master_state is ( SM4_idle, SM4_state1,SM4_state1a,SM4_state2,SM4_state2a,SM4_state3, SM4_state4,
									SM4_state5,SM4_state6,SM4_state6a,SM4_state7,SM4_state7a,SM4_state8,SM4_state9,
									SM4_state10,SM4_state11,SM4_state12,SM4_state13,
									SM4_READ,SM4_stateR1,SM4_stateR2,SM4_stateR3,SM4_stateR4);
                        
								
  signal i_start: std_logic;
  signal i_stop: std_logic;
  signal c_stop: std_logic; --counter based STOP
  signal c_stop_latched: std_logic;
  signal i_write: std_logic;
  signal i_read: std_logic;
  signal i_ROM_end: std_logic :='0';					
  signal stm_SM4 : SM4_master_state;
  signal i_enCamCount: std_logic; -- run only after a reset
  signal i_cnt: natural range 0 to 7:=1 ;
  signal i_resetZ_cnt: natural range 0 to 400:=0 ; -- ensure camera reset is 1ms long
  signal i_resetZ_cnt_en:std_logic:='0';
  signal i_go_latched:std_logic:='0'; -- a i2c go signal controlled by the reset system (with 1ms delay)
  signal  i_cnt_en: std_logic:='0';
  constant Tcnt: natural :=3; -- no. of ROM entries for Camera init
  signal i_gap_cnt: natural range 0 to 4096:=0 ;
  signal  i_gap_cnt_end: std_logic:='0';
  signal i_vcm_cnt: natural range 0 to 255:=0 ; --count at 400KHz
  signal  i_vcm_cnt_end: std_logic:='0';
  
  constant i_gap_cnt_Tcnt: natural :=3500; -- no. of ROM entries for Camera init
  constant i_vcm_cnt_Tcnt: natural :=100; -- no. of ROM entries for Camera init
begin 

--======================== TC from counter converted to latched '1', only '0' on reset

read<=i_read; -- i2c read ID only
write<=i_write;-- and not(c_stop); -- ctrl from SM4 below
ROM_end_o<=i_ROM_end;
enCamCount<=i_enCamCount;
--ROM_CLK <= i_start OR i_enCamCount;
ROM_CLK <= i_enCamCount;
start<=i_start;
stop<= ROM_end or c_stop;
--stop<=i_stop;
process (clk,rst, ROM_end,stm_SM4)
begin
	if (rst='1' or stm_SM4=SM4_state7) then
		i_ROM_end<='0';

	elsif (clk'event and clk='1' and ROM_end='1') then
		i_ROM_end <= '1';
		end if;

end process;

-- 1ms counter to drive camera reset
process (clk_0400,rst)
begin
	if (rst='1') then -- sync reset when NOT system reset
		i_go_latched <='0'; -- a latched go signal for the i2c system
		i_resetZ_cnt<=0;  -- reset count value
		i_resetZ_cnt_en<='1'; -- enable count
		resetZ<='1';
	elsif (clk_0400'event and clk_0400='1') then
		if (i_resetZ_cnt_en='1') then
			i_resetZ_cnt<= i_resetZ_cnt+1;
			resetZ<='0';
			i_go_latched <='0'; -- a latched go signal for the i2c system
			
			if (i_resetZ_cnt > 400) then
				resetZ<='1';
				i_go_latched <='1'; -- a latched go signal for the i2c system
				i_resetZ_cnt<=0;
				i_resetZ_cnt_en<='0'; -- disable the counter until the next reset
			end if;
		else
			resetZ<='1';
			i_go_latched <='0'; -- a latched go signal for the i2c system
			i_resetZ_cnt<=0;
		end if;
	end if;
end process;
--==================== STOP from i_cnt count loop, latched here, reset by Sm4_state6a
--==================== used to end 4-phase write
process (clk,rst, c_stop, stm_SM4)
begin
	if (rst='1' or stm_SM4=SM4_idle or i_gap_cnt_end='1') then
		c_stop_latched <='0';
	elsif (clk'event and clk='1' and c_stop='1') then
		c_stop_latched <= '1';
	end if;
end process;


-- 3 step counter, counts through the addr, sub addr and data bytes
process (clk,rst, ROM_end,stm_SM4, i_cnt_en, rom_addr_incr)
begin
	if (rst='1') then
		i_cnt<=0;
		c_stop<='0';
	elsif ( stm_SM4=SM4_state6a) then
		i_cnt<=1;
		c_stop<='0';
	elsif (clk'event and clk='1' and  i_cnt_en='1') then --- was... and rom_addr_incr='1') then
		if (i_cnt<Tcnt) then
			i_cnt <= i_cnt+1;
			c_stop<='0';
		else
			i_cnt <= 0;
			c_stop<='1';
		end if;
	end if;
end process;

-- i2c gap in transmission counter, counts 512 at 25MHz
process (clk,rst, ROM_end,stm_SM4, i_cnt_en, rom_addr_incr)
begin
	if (rst='1' or i_start='1') then
		i_gap_cnt<=0;
		i_gap_cnt_end<='0';
	elsif (clk'event and clk='1' and  c_stop_latched='1') then --- was... and rom_addr_incr='1') then
		if (i_gap_cnt<i_gap_cnt_Tcnt) then
			i_gap_cnt <= i_gap_cnt+1;
			i_gap_cnt_end<='0';
		else
			i_gap_cnt <= 0;
			i_gap_cnt_end<='1';
		end if;
	end if;
end process;

-- i2c gap in VCM transmissions, slow to 20ms repeat,  counter counts 512 at 400KHz
process (clk_0400,rst, rom_addr_incr, i_start)
begin
	if (rst='1' or i_start='1') then
		i_vcm_cnt<=0;
		i_vcm_cnt_end<='0';
	elsif (clk_0400'event and clk_0400='1') then
		if (i_vcm_cnt<i_vcm_cnt_Tcnt) then
			i_vcm_cnt <= i_vcm_cnt+1;
			i_vcm_cnt_end<='0';
		else
			i_vcm_cnt <= 0;
			i_vcm_cnt_end<='1';
		end if;
	end if;
end process;

--=========================================
-- State machine for controlling i2c master
--=========================================
process (clk, rst)
begin
	  if (i_resetZ_cnt_en='1') then
			sel<= '0';
			i_start<='0';
			i_stop <='0';
			i_write <='0';
i_read <='0';
			i2c_reset<='1';
			i_cnt_en<='0';
			i_enCamCount<='0';
			stm_SM4<=SM4_idle; --  stm_SM4
	elsif (clk ='1' and clk'event) then
		i2c_reset<='0';
		case stm_SM4 is
		--============================== IDLE
		when SM4_idle =>
		
				--if (go='1' and read_ID='0') then  
				if (i_go_latched='1' and read_ID='0') then  
				
				i_start <='1';
				i_stop <='0';
				i_write <='0';
i_read <='0';
				--i2c_reset<='0';
				i_enCamCount <='1';
				i_cnt_en<='0';
				sel<= '0';
				stm_SM4<= SM4_state1; --skip 1 and 2
				--elsif (go='1' and read_ID='1') then  
				elsif (i_go_latched='1' and read_ID='1') then  
				i_start <='1';
				i_stop <='0';
				i_write <='0';
i_read <='0';
				--i2c_reset<='0';
				i_enCamCount <='1';
				i_cnt_en<='0';
				sel<= '0';
				stm_SM4<= SM4_READ; --read ID byte
				
				else
				i_start <='0';
				i_stop <='1';
				i_write <='0';
i_read <='0';
				sel<= '0';
				i_enCamCount <='0';
				i_cnt_en<='0';
				stm_SM4<= SM4_idle;
				end if;	
		--==============================  a 4 phase WRITE, first wait 1
			  when SM4_state1 => 
				i_start <='0';
				i_stop <='0';
				i_write <='0';
i_read <='0';
				sel<= '0';
				i_enCamCount <='0';
				i_cnt_en<='1';
				stm_SM4<= SM4_state2;
			  when SM4_state2 => 
				i_start <='0';
				i_stop <='0';
				i_write <='0';
i_read <='0';
				sel<= '0';
				i_enCamCount <='0';
				i_cnt_en<='0';
				stm_SM4<= SM4_state3;
				--========================== write assert, select ROM addr 00, start i2c TX
			  when SM4_state3 => -- wait
				if (ROM_addr_incr='1') then  
				i_start <='0';
				i_stop <='0';
				i_write <='1';
i_read <='0';
				i_enCamCount <='0';
				sel<= '0';
				i_cnt_en<='0';
				stm_SM4<= SM4_state3;
				else
				i_start <='0';
				i_stop <='0';
				i_write <='1';
i_read <='0';
				sel<= '0';
				i_enCamCount <='0';
				i_cnt_en<='0';
				stm_SM4<= SM4_state4;
				end if;
				--======================================================================
			  when SM4_state4 => -- send sub-addr tocamera
				if (ROM_addr_incr='0') then --- wait here for 
				i_start <='0';
				i_stop <='0';
				i_write <='1';
i_read <='0';
				i_enCamCount <='0';
				sel<= '0';
				i_cnt_en<='0';
				stm_SM4<= SM4_state4;
				
				else
				i_start <='0';
				i_stop <='1';
				i_write <='1';
i_read <='0';
				sel<= '0';
				i_enCamCount <='1';
				i_cnt_en<='0';
				stm_SM4<= SM4_state5;
				end if;
				--================================= end of 4 phase data send, 
				--================================= go round again if more in ROM
			  when SM4_state5 => 
				if (c_stop_latched='1') then --- re-start to programme next register in a new 4-phase write cycle
				i_start <='0';
				i_stop <='1';
				i_write <='0';
i_read <='0';
				i_enCamCount <='0';
				sel<= '0';
				i_cnt_en<='0';
				stm_SM4<= SM4_state6;
				elsif (i_ROM_end='1') then --- incr. MUX and go to VCM control
				i_start <='0';
				i_stop <='1';
				i_write <='0';
i_read <='0';
				i_enCamCount <='0';
				sel<= '0';
				i_cnt_en<='0';
				stm_SM4<= SM4_state7;
				else
				i_start <='0';
				i_stop <='0';
				i_write <='1';
i_read <='0';
				sel<= '0';
				i_enCamCount <='0';
				i_cnt_en<='0';
				stm_SM4<= SM4_state1; --======= send next of 4 bytes
				end if;
				--================================ STATE6 
			  when SM4_state6 => -- re-start for next 4 phase write
				if (i_gap_cnt_end='0') then --- wiat here for timer to make gap for nect TX		  
				i_start <='0';
				i_stop <='1';
				i_write <='0';
i_read <='0';
				i_enCamCount <='0';
				sel<= '0';
				i_cnt_en<='0';
				stm_SM4<= SM4_state6;
				else
				i_start <='0';
				i_stop <='0';
				i_write <='0';
i_read <='0';
				i_enCamCount <='0';
				sel<= '0';
				i_cnt_en<='0';
				stm_SM4<= SM4_state6a;
				end if;
			  when SM4_state6a => -- re-start for next 4 phase write
				i_start <='1';
				i_stop <='0';
				i_write <='0';
i_read <='0';
				i_enCamCount <='0';
				sel<= '0';
				i_cnt_en<='0';
				stm_SM4<= SM4_state2; -- dont increment the rom addr counter, as already at the right addr.
			  when SM4_state7 => -- send data
			  if (ROM_END='1') then
				i_start <='0';
				i_stop <='1';
				i_write <='0';
i_read <='0';
				sel<= '1';
				i_enCamCount <='0';
				i_cnt_en<='0';
				stm_SM4<= SM4_state7;
				else
				i_start <='0';
				i_stop <='0';
				i_write <='1';
i_read <='0';
				sel<= '1';
				i_enCamCount <='0';
				i_cnt_en<='0';
				stm_SM4<= SM4_state7a;
				end if;
			  when SM4_state7a => --  go here when  ROM_END is a 0 again. 
				i_start <='0';
				i_stop <='0';
				i_write <='1'; -- start cycle again
i_read <='0';
				sel<= '1';
				i_enCamCount <='0';
				i_cnt_en<='0';
--				stm_SM4<= SM4_state8;			
				stm_SM4<= SM4_idle;	--=================== JUST IDLE		
				
				--========================================= now for the VCM control,
				-- note that state7 clears the ROM_END flag also
				-- format  Addr:data hi byte:data lo byte
				-- cycle through this continuously until reset
			  when SM4_state8 => -- ADDR
				i_cnt_en<='0';
				if (ROM_addr_incr='1') then --- when written, loop back to send the next cmd tro the VCM 
				i_start <='0';
				i_stop <='0';
		i_write <='1';
i_read <='0';
				i_enCamCount <='0';
				sel<= '1';
				stm_SM4<= SM4_state8;
				else
				i_start <='0';
				i_stop <='0';
		i_write <='1';
i_read <='0';
				sel<= '1';
				i_enCamCount <='0';
				stm_SM4<= SM4_state9;
				end if;
			  when SM4_state9 => -- wait for Rom ack
i_cnt_en<='0';
				if (ROM_addr_incr='0') then  
				i_start <='0';
				i_stop <='0';
		i_write <='1';
i_read <='0';
				i_enCamCount <='0';
				sel<= '1';
				stm_SM4<= SM4_state9;
				else
				i_start <='0';
				i_stop <='0';
		i_write <='1';
i_read <='0';
				sel<= '1';
				i_enCamCount <='0';
				stm_SM4<= SM4_state10;
				end if;
				
			  when SM4_state10 => -- DATA HI
i_cnt_en<='0';
				if (ROM_addr_incr='1') then --- when written, loop back to send the next cmd tro the VCM 
				i_start <='0';
				i_stop <='0';
		i_write <='1';
i_read <='0';
				i_enCamCount <='0';
				sel<= '1';
				stm_SM4<= SM4_state10;
				else
				i_start <='0';
				i_stop <='0';
				i_write <='1';
i_read <='0';
				sel<= '1';
				i_enCamCount <='0';
				stm_SM4<= SM4_state11;
				end if;
				when SM4_state11 => -- wait for Rom ack
				i_cnt_en<='0';
				if (ROM_addr_incr='0') then  
				i_start <='0';
				i_stop <='0';
				i_write <='1';
i_read <='0';
				i_enCamCount <='0';
				sel<= '1';
				stm_SM4<= SM4_state11;
				else
				i_start <='0';
				i_stop <='0';
				i_write <='1';
i_read <='0';
				sel<= '1';
				i_enCamCount <='0';
				stm_SM4<= SM4_state12;
				end if;
			  when SM4_state12 => -- DATA LOW
				i_cnt_en<='0';
				if (ROM_addr_incr='1') then --- when written, loop back to send the next cmd tro the VCM 
				i_start <='0';
				i_stop <='0';
				i_write <='1';
i_read <='0';
				i_enCamCount <='0';
				sel<= '1';
				stm_SM4<= SM4_state12;
				else
				i_start <='0';
				i_stop <='1';
				i_write <='1';
i_read <='0';
				sel<= '1';
				i_enCamCount <='0';
				stm_SM4<= SM4_state13;
				end if;
				when SM4_state13 => -- wait for Rom ack
				i_cnt_en<='0';
				if (ROM_addr_incr='0') then  
				i_start <='0';
				i_stop <='1'; -- end of three byte seq. send stop.
				i_write <='0';
i_read <='0';
				i_enCamCount <='0';
				sel<= '1';
				stm_SM4<= SM4_state13;
				else
				i_start <='0';
				i_stop <='1';
				i_write <='0';
i_read <='0';
				sel<= '1';
				i_enCamCount <='0';
				stm_SM4<= SM4_state7a; -- go round again infinite while loop, broken by reset button
				end if;
				
--================== READ ID BYTe
				when SM4_READ=> 
				i_start <='0';
				i_stop <='0';
				i_write <='0';
i_read <='1';
				sel<= '0';
				i_enCamCount <='0';
				i_cnt_en<='1';
				stm_SM4<= SM4_stateR2;
			  when SM4_stateR2 => 
				i_start <='0';
				i_stop <='0';
				i_write <='0';
i_read <='0';
				sel<= '0';
				i_enCamCount <='0';
				i_cnt_en<='0';
				stm_SM4<= SM4_stateR3;
				--========================== write assert, select ROM addr 00, start i2c TX
			  when SM4_stateR3 => -- wait
				if (ROM_addr_incr='1') then  
				i_start <='0';
				i_stop <='0';
				i_write <='0';
i_read <='1';
				i_enCamCount <='0';
				sel<= '0';
				i_cnt_en<='0';
				stm_SM4<= SM4_stateR3;
				else
				i_start <='0';
				i_stop <='0';
				i_write <='0';
i_read <='1';
				sel<= '0';
				i_enCamCount <='0';
				i_cnt_en<='0';
				stm_SM4<= SM4_stateR4;
				end if;
				--======================================================================
			  when SM4_stateR4 => -- send sub-addr tocamera
				if (ROM_addr_incr='0') then --- wait here for 
				i_start <='0';
				i_stop <='0';
				i_write <='0';
i_read <='1';
				i_enCamCount <='0';
				sel<= '0';
				i_cnt_en<='0';
				stm_SM4<= SM4_stateR4;
				
				else
				i_start <='0';
				i_stop <='1';
				i_write <='0';
i_read <='1';
				sel<= '0';
				i_enCamCount <='1';
				i_cnt_en<='0';
				stm_SM4<= SM4_idle;
				end if;
				--================================= end of 4 phase data send, 

			when others=>
i_cnt_en<='0';
				i_start <='0';
				i_stop <='1';
		i_write <='0';
i_read <='0';
				sel<= '1';
				i_enCamCount <='0';
				stm_SM4<= SM4_idle;
	
		end case;
	  end if;
 end process;
  
end architecture;