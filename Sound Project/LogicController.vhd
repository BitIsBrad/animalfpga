LIBRARY ieee;
USE ieee.std_logic_1164.all;
USE ieee.std_logic_arith.all;
USE ieee.std_logic_unsigned.all; --PC added

ENTITY LogicController IS
  
   PORT( 
      --INPUTS
		Clock400K : IN std_logic;
		DistanceA : IN integer range 0 to 10000;
		DistanceB : IN integer range 0 to 10000;
		VisionPos : IN integer range -10 to 10;
		VisionSize : IN integer range 0 to 4;
		VisionFound : IN std_logic;
		--OUTPUTS
		ResetA : OUT std_logic := '0';
		ResetB : OUT std_logic := '0';
		SpeedA : OUT integer range -200 to 200 := 0;
		SpeedB : OUT integer range -200 to 200 := 0;
		PlaySound : OUT std_logic := '0';
		PlayMode : OUT std_logic := '0'
	);

END LogicController;

architecture LogicControllerV1 of LogicController is
	type states is (spin, pause, reset, shuffle, chase);
	signal CurrentState : states := reset;
	signal Direction : std_logic := '0';
	signal ShuffleToggle : std_logic := '0';
	shared variable Speed : integer range -200 to 200 := 75;
	shared variable Distance : integer := 55;
	shared variable PauseCounter : integer range 0 to 8388607 := 300000;
	
	shared variable Seed : integer range 0 to 65535 := 1234;

begin
	
	process (Clock400K)
	begin
		if (rising_edge(Clock400K)) then
			case CurrentState is
				when spin =>
					--Spin
					
					--Command audio unit to make sound
					PlayMode <= '0';
					PlaySound <= '1';
					
					--Turn the motors in oposite directions
					if (Direction = '1') then
						SpeedA <= speed;
						SpeedB <= -speed;
					else
						SpeedA <= -speed;
						SpeedB <= speed;
					end if;
					
					--Prevent the sensors from resetting
					ResetA <= '0';
					ResetB <= '0';
					
					if (DistanceA >= Distance) and (DistanceB >= Distance) then	
						PlaySound <= '0';
					
						--Make random pause time
						Seed := (1242 * Seed + 1234) mod 3452545;
						PauseCounter := 300000 + (Seed * 27);		
						
						CurrentState <= pause;
					
					elsif (VisionFound = '1') then
						PlaySound <= '0';
					
						--Make timings for shuffle
						Seed := (1242 * Seed + 1234) mod 3452545;
						PauseCounter := 18000 + (Seed * 8);
						CurrentState <= shuffle;	
						 
					end if;
				
				when pause =>
				
--					--Stop the motors
					SpeedA <= 0;
					SpeedB <= 0;
				
					--Pause for time
					PauseCounter := PauseCounter - 1;
					if (PauseCounter = 0) then
					
						CurrentState <= reset;
						
					end if;
				when reset =>
					--Reset both counters
					
					--Stop the motors
					SpeedA <= 0;
					SpeedB <= 0;
					
					--Start the reset process
					ResetA <= '1';
					ResetB <= '1';
						
					--Wait for sensors to be reset
					if (DistanceA = 0) and (DistanceB = 0) then
					
						--Make new random speed and distance
						Seed := (1242 * Seed + 1234) mod 3452545;
						Speed := 40 + (Seed / 1300);
						
						Seed := (1242 * Seed + 1234) mod 3452545;
						Distance := 20 + (Seed / 800);
						
						--Change the spin direction
						if (Direction = '1') then
							Direction <= '0';
						else 
							Direction <= '1';
						end if;
						 
						--Wait some time and then transition
						CurrentState <= spin;
					end if;
					
				when shuffle =>
					--Shuffle/Stutter forward
				
					--Quick on off motor control
					PauseCounter := PauseCounter - 1;
					
					if (PauseCounter = 0) then
							SpeedA <= 0;
							SpeedB <= 0;
							CurrentState <= chase;
					elsif (PauseCounter mod 25000) = 0 then
						if ShuffleToggle = '0' then
							ShuffleToggle <= '1';
							SpeedA <= 120;
							SpeedB <= 120;
						else
							ShuffleToggle <= '0';
							SpeedA <= 0;
							SpeedB <= 0;
						end if;
					end if;
					
				when chase =>
					--Chase after target
					
					PlayMode <= '1';
					PlaySound <= '1';
					
					if (VisionFound = '1') then
						
						PauseCounter := 15000;
						
					elsif (PauseCounter > 0) then
					
						PauseCounter := PauseCounter - 1;
						
					end if;
					
					if (PauseCounter = 0) then
					
						PlaySound <= '0';
					
						CurrentState <= reset;

					else
					
						if (VisionPos = 0) then
							--Go forwards
							SpeedA <= 120;
							SpeedB <= 120;
						elsif (VisionPos > 0) then
							--Go right
							SpeedA <= 120;
							SpeedB <= 120 - (VisionPos * 10);
						else
							--Go left
							SpeedA <= 120 + (VisionPos * 10);
							SpeedB <= 120;
						end if;
					

					end if;

					
			end case;
		end if;
	end process;

end LogicControllerV1;